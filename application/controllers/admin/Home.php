<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->library('cart');
		$this->load->helper(array('form', 'url'));  
	}

	public function index()
	{		
		$data['all_user'] = $this->db->count_all_results('akun'); 
		$data['all_user_today'] = $this->db->where('created_at', date('Y-m-d'))->count_all_results('akun');
		$data['user_register_today'] = $this->db->where('created_at', date('Y-m-d'))->get('akun')->result();
		$this->load->view('admin/dashboard', $data);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
