<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('cart');
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{		
		if ($this->session->userdata('isLogedIn')) {
			redirect(site_url('admin/home'));
		}
		$this->load->view('admin/login');
	}


	public function doLogin()
	{
		if ($this->session->userdata('isLogedIn')) {
			redirect(site_url('admin/home'));
		}	

		  $login = $this->db->where(['email' => $this->input->post('email'),'password' => $this->input->post('password') ])->get('admin');
		  if ($login->result()) {
		  	$data = $login->row();  
		  		$sessionData = array(
		  							 'email' => $data->email,
		  							 'user_name' => $data->username,
		  							 'id_akun' =>  $data->id
		  							);
				$this->session->set_userdata('isLogedIn', $sessionData);
				redirect(site_url('admin/home/'));

		  }else{
				redirect(site_url('admin/akun'));
		  }

	}

	public function log_out()
	{
		$this->session->sess_destroy();
		redirect(site_url('admin/akun'));
	}


}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
