<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->library('cart');
		$this->load->helper(array('form', 'url'));  
	}

	public function lomba()
	{		
		$data['lomba'] = $this->db->get('kategori_lomba')->result();
		$this->load->view('admin/setting/lomba/index', $data);
	}

	public function createCategori()
	{		
		$this->load->view('admin/setting/lomba/create');
	}



	public function actionCategory()
	{
		$this->db->insert('kategori_lomba', $this->input->post());
		$this->session->set_flashdata('alert', 'Tambah Data Sukses');
		redirect(site_url('admin/setting/lomba'));
	}

	public function edit_lomba($id)
	{		
		$data['categori'] = $this->db->where('id', $id)->get('kategori_lomba')->result();
		$this->load->view('admin/setting/lomba/edit', $data);
	}

	public function update_category()
	{		
		$data['categori'] = $this->db->where('id', $this->input->post('id'))->update('kategori_lomba', ['nama_kategori' => $this->input->post('nama_kategori')]);
		redirect(site_url('admin/setting/lomba'));
	}

	public function delete_kategori($id)
	{		
		$data['categori'] = $this->db->where('id', $id)->delete('kategori_lomba');
		redirect(site_url('admin/setting/lomba'));
	}

	public function user()
	{		
		$data['user'] = $this->db->get('admin')->result();
		$this->load->view('admin/akun/index', $data);
	}


	public function create_user()
	{		
		$this->load->view('admin/akun/create');
	}

	public function edit_user($id)
	{		
		$data['user'] = $this->db->where('id', $id)->get('admin')->result();
		$this->load->view('admin/akun/edit', $data);
	}

	public function save_user()
	{		
		$this->db->insert('admin', $this->input->post());
		redirect(site_url('admin/setting/user'));
	}


	public function update_user()
	{		
		$this->db->where('id', $this->input->post('user_id'))->update('admin', ['username' => $this->input->post('username'),'email' => $this->input->post('email'),'password' => $this->input->post('password')]);
		redirect(site_url('admin/setting/user'));
	}





}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
