<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftar extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->library('cart');
		$this->load->helper(array('form', 'url'));  
	}

	public function index()
	{	
		$data['pendaftar'] = $this->db->get('akun')->result();
		$this->load->view('admin/pendaftar/index', $data);
	}

	public function detail($id)
	{	
		$data['kategori'] = $this->db->get('kategori_lomba')->result();
		$data['pendaftar'] = $this->db->where('id', $id)->get('akun')->result();
		$this->load->view('admin/pendaftar/detail', $data);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
