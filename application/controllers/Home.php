<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('cart');
		$this->load->helper(array('form', 'url'));  
	}

	public function index()
	{		
		$data['kategori'] = $this->db->get('kategori_lomba')->result();
		$this->load->view('home/index', $data);
	}

	public function register()
	{
		$id_akun = array('id_akun' => $this->codeRegister(), 'created_at'=> date('Y-m-d') );
		$data = array_merge($id_akun, $this->input->post());

		$cek =  $this->db->where('nik',$this->input->post('nik'))->get('akun');
		if ($cek->num_rows() <> 0) {
			$data['cek'] =  $this->db->where('nik',$this->input->post('nik'))->get('akun')->row();
			return $this->load->view('home/register', $data);
		}

		$insert = $this->db->insert('akun', $data);
		$this->session->set_flashdata('data_akun', $data);
		redirect(site_url('home/registerPage'));
	}

	public function registerPage()
	{
		$data['cek'] = [];
		$this->load->view('home/register', $data);
	}

	private function codeRegister()
	{
	  $this->db->select('id as id_akun')->order_by('id', 'DESC')->limit(1);
	  $query = $this->db->get('akun');
	  if ($query->num_rows() <> 0) {
		    $data = $query->row();   
		    $kode = intval($data->id_akun) + 1; 
	  }else{
		    $kode = 1; 
	  }
	  return $kodemax = "NO-".$kode; 	
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
