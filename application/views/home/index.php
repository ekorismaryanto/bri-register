<!DOCTYPE html>
<html lang="en">
<head>
  <title>Your Aplication</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/home'; ?>/images/icons/favicon.ico"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/animate/animate.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/css/util.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/css/main.css">
<!--===============================================================================================-->
</head>
<body>
  
  
  <div class="container-login100" style="background-image: url('<?php echo base_url().'assets/home'; ?>/images/bg-01.jpg');">
    <div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
      <form class="login100-form validate-form" action="<?php echo site_url('home/register') ?>" method="post">
        <span class="login100-form-title p-b-37">
          Registrasi
        </span>

        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Nik">
          <input class="input100" type="number" name="nik" placeholder="NIK">
          <span class="focus-input100"></span>
        </div>

        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Name">
          <input class="input100" type="text" name="nama" placeholder="nama lengkap">
          <span class="focus-input100"></span>
        </div>

        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Phone Number">
          <input class="input100" type="text" name="no_hp" placeholder="Phone Number(HP)">
          <span class="focus-input100"></span>
        </div>

        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Adress">
          <input class="input100" type="text" name="alamat" placeholder="alamat">
          <span class="focus-input100"></span>
        </div>

        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Age">
          <input class="input100" type="number" name="usia" placeholder="umur">
          <span class="focus-input100"></span>
        </div>

        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Sex">
          <select class="form-control" name="jenis_kelamin">
            <option value="" selected="">Pilih Jenis Kelamin</option>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
          </select>  
        </div>

        <div class="form-group t-left">
          <select class="form-control" name="kategori_lomba">
            <option value="" selected="">Pilih Jenis Lomba</option>
            <?php foreach ($kategori as $row) { ?>
              <option value="<?php echo $row->nama_kategori ?>"><?php echo $row->nama_kategori ?></option>
            <?php } ?>
          </select>  
        </div>

        <div class="container-login100-form-btn">
          <button class="login100-form-btn" type="submit">
           register
          </button>
        </div>

        <div class="text-center p-t-57 p-b-20">
          <span class="txt1">
            Support By
          </span><br>
              <img src="https://mmc.tirto.id/image/otf/500x0/2016/05/11/LogoBRI_050_ratio-16x9.JPG" alt="bri" width="50%" >
        </div>
      </form>      
    </div>
  </div>
    
<!--===============================================================================================-->
  <script src="<?php echo base_url().'assets/home/'; ?>/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url().'assets/home/'; ?>/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url().'assets/home/'; ?>/vendor/bootstrap/js/popper.js"></script>
  <script src="<?php echo base_url().'assets/home/'; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  
<!--===============================================================================================-->
  <script src="<?php echo base_url().'assets/home/'; ?>/vendor/daterangepicker/moment.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url().'assets/home/'; ?>/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url().'assets/home/'; ?>/js/main.js"></script>

</body>
</html>