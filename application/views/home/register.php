<!DOCTYPE html>
<html lang="en">
<head>
  <title>Your Aplication</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/home'; ?>/images/icons/favicon.ico"/>
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/fonts/iconic/css/material-design-iconic-font.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/animate/animate.css">
  <!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/vendor/animsition/css/animsition.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/css/util.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/home'; ?>/css/main.css">
  <!--===============================================================================================-->
</head>
<body>

  <?php
  $data = [];
  $message = $this->session->flashdata('data_akun');
  $data = $cek;
  ?>

    <style type="text/css">
    * {
        -webkit-print-color-adjust: exact !important; /*Chrome, Safari */
        color-adjust: exact !important;  /*Firefox*/
    }
    .t-left{
        text-align: left !important
    }
    .t-center{
        text-align: center !important
    }
   
    @media print {
       
      .nganu{
        background-image: url('http://localhost:1234/assets/images/background-pendaftaran.png');
        width: 40%;
        height: 500px;
        margin-left: 2px;
      }
      .mobile{
        padding-left: 35px;
      }
      .btn-simpan{
        padding-left: 10%;
          margin-bottom: 30px;
      }

      @media (max-width: 480px){
        .nganu {
          background-image: url('http://localhost:1234/assets/images/background-pendaftaran-mobile.png');
          width: 700px;
          height: 650px;
        }
        .mobile{
          width: 200px;
        }
        .btn-simpan{
          padding-left: 20px;
          margin-bottom: 30px;
        }
      }
    }
    
    #kiri
    {
      width:50%;
      float:left;
    }

    #kanan
    {
      width:50%;
      height:100px;
      float:right;
    }
    .nganu{
        background-image: url('http://localhost:1234/assets/images/background-pendaftaran.png');
        width: 40%;
        height: 400px;
        margin-left: 2px;
    }
    .mobile{
      padding-left: 35px;
    }
    .btn-simpan{
      padding-left: 20%;
        margin-bottom: 30px;

    }
    @media (max-width: 480px){
      #kiri
      {
        width:100%;
        float:left;
      }

      #kanan
      {
        width:100%;
        height:100px;
        float:right;
      }
      .nganu {
        background-image: url('http://localhost:1234/assets/images/background-pendaftaran-mobile.png');
        width: 100%;
        height: 650px;
      }
      .mobile{
        width: 200px;
      }
      .btn-simpan{
        padding-left: 20px;
        margin-bottom: 30px;
      }
    }
  </style>

  <div class="container-login100" style="background-image: url('<?php echo base_url().'assets/home'; ?>/images/bg-01.jpg');">
    <div class="wrap-login100 nganu">
      <div style="margin-top: 12%;">
          <div id="kiri">
              <div class="mobile" style="font-size: 12px;text-align: center;">
                <div>
                    <h3><b>No Pendaftaran</b></h3>
                    <hr>
                    <h3><?php if( count($data) > 0 ) { echo $data->id_akun; }else{ echo $message['id_akun']; } ?></h3>
                    <p>Hai <?php echo $message['nama'] ?>, harap simpan baik-baik nomer pendaftaran.</p>
                </div>
              </div>
              <div onclick="myFunction()"  class="btn-simpan">
                <button class="btn btn-primary" style="font-size: 11px">
                 SIMPAN / PRINT NOMER PENDAFTARAN
               </button>
             </div>
          </div>
        
          <div id="kanan" style=""></div>
      </div>
    </div>
  </div>

  <script>
    function myFunction() {
      window.print();
    }
  </script>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="<?php echo base_url().'assets/home/'; ?>/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url().'assets/home/'; ?>/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url().'assets/home/'; ?>/vendor/bootstrap/js/popper.js"></script>
<script src="<?php echo base_url().'assets/home/'; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
<script src="<?php echo base_url().'assets/home/'; ?>/vendor/daterangepicker/moment.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url().'assets/home/'; ?>/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url().'assets/home/'; ?>/js/main.js"></script>

</body>
</html>