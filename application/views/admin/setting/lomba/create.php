<?php

$this->load->view('admin/component/header');
$this->load->view('admin/component/sidebar');
$this->load->view('admin/component/top_nav');

?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">


    <div class="col-md-6 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Tambah Kategori</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left" method="post" action="<?php echo site_url('admin/setting/actionCategory') ?>">

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Katgori</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" placeholder="Kategori" name="nama_kategori">
              </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <!-- <button type="button" class="btn btn-primary">Cancel</button> -->
                <!-- <button type="reset" class="btn btn-primary">Reset</button> -->
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('admin/component/footer'); ?> 
