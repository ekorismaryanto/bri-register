<?php

$this->load->view('admin/component/header');
$this->load->view('admin/component/sidebar');
$this->load->view('admin/component/top_nav');

?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Master Data Kategori Lomba <small> <a href="<?php echo site_url('admin/setting/createCategori') ?>"><button class="btn btn-danger">Tambah kategori</button></a> </small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($lomba as $row) {?>
              <tr>
                <td><?php echo $row->nama_kategori ?></td>
                <td>
                  <a href="<?php echo site_url('admin/setting/edit_lomba/'. $row->id) ?>"> <span class="fa fa-pencil"></span> Edit</a> <br>
                  <a href="<?php echo site_url('admin/setting/delete_kategori/'. $row->id) ?>"> <span class="fa fa-trash"></span> Hapus</a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('admin/component/footer'); ?> 
