<!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
          <li><a href="<?php echo site_url('admin/home'); ?>"><i class="fa fa-laptop"></i> Home</a></li>
          <li><a href="<?php echo site_url('admin/pendaftar'); ?>"><i class="fa fa-user"></i> Pendaftar</a></li>
          <li><a><i class="fa fa-desktop"></i> Setting <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="<?php echo site_url('admin/setting/lomba') ?>">Lomba</a></li>
                  <!-- <li><a href="general_elements.html">Website</a></li> -->
                </ul>
          </li>
          <li><a href="<?php echo site_url('admin/setting/user'); ?>"><i class="fa fa-user"></i> Akun</a></li>
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>