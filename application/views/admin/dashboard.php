<?php

$this->load->view('admin/component/header');
$this->load->view('admin/component/sidebar');
$this->load->view('admin/component/top_nav');

?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">


    <div class="x_panel">
      <!-- top tiles -->
      <div class="row tile_count">
        <div class="col-md-4">
          <div class="x_panel">
            <div class="x_title">
              <h2>User Register Today</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <?php foreach($user_register_today as $row) { ?>
              <article class="media event">
                <a class="pull-left date">
                  <p class="month"><?php echo date('F') ?></p>
                  <p class="day"><?php echo date('d') ?></p>
                </a>
                <div class="media-body">
                  <a class="title" href="#"><?php echo $row->id_akun ?></a>
                  <p><?php echo $row->nama ?></p>
                </div>
              </article>
            <?php } ?>

            </div>
          </div>
        </div>

        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-user"></i></div>
            <div class="count"><?php echo $all_user_today ?></div>
            <p>User Register Hari Ini</p>
          </div>
        </div>

        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-users"></i></div>
            <div class="count"><?php echo $all_user ?></div>
            <p>Total Register</p>
          </div>
        </div>

      </div>
      <!-- /top tiles -->
    </div>
  </div>
</div>
<?php $this->load->view('admin/component/footer'); ?> 