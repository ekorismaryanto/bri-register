<?php

$this->load->view('admin/component/header');
$this->load->view('admin/component/sidebar');
$this->load->view('admin/component/top_nav');

?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Master Data user Admin <small> <a href="<?php echo site_url('admin/setting/create_user') ?>"><button class="btn btn-danger">Tambah User</button></a> </small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($user as $row) {?>
              <tr>
                <td><?php echo $row->username ?></td>
                <td><?php echo $row->email ?></td>
                <td>
                  <a href="<?php echo site_url('admin/setting/edit_user/'.$row->id) ?>"> <span class="fa fa-pencil"></span> Edit</a> 
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('admin/component/footer'); ?> 
