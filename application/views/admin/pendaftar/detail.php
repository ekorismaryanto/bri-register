<?php
$this->load->view('admin/component/header');
$this->load->view('admin/component/sidebar');
$this->load->view('admin/component/top_nav');
?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">

    <div class="col-md-6 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Detail Peserta <button id="doPrint" class="btn btn-danger btn-sm"> <span class="fa fa-print"></span> Print</button></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left" method="post" action="<?php echo site_url('admin/setting/actionCategory') ?>">
            <div id="printDiv">
              <h4 for="nik" >No Pendaftaran : <?php echo $pendaftar[0]->id_akun ?></h4>
              <div class="form-group t-left">
                <label for="nik" >Nik:</label>
                <input type="number" class="form-control" id="nik" required="" name="nik" value="<?php echo $pendaftar[0]->nik ?>">
              </div>

              <div class="form-group t-left">
                <label for="nama" >Nama:</label>
                <input type="text" class="form-control" id="nama" required="" name="nama" value="<?php echo $pendaftar[0]->nama ?>">
              </div>

              <div class="form-group t-left">
                <label for="no_hp" >No HP:</label>
                <input type="number" class="form-control" id="nama" required="" name="no_hp" value="<?php echo $pendaftar[0]->no_hp ?>">
              </div>

              <div class="form-group t-left">
                <label for="Alamat" >Alamat:</label>
                <textarea class="form-control" required="" name="alamat"><?php echo $pendaftar[0]->alamat ?></textarea>
              </div>

              <div class="form-group t-left">
                <label for="usia" >Usia:</label>
                <input type="number" class="form-control" required="" name="usia" value="<?php echo $pendaftar[0]->usia ?>">
              </div>

              <div class="form-group t-left" >
                <label for="pwd">Jenis Kelamin:</label>
                <select class="form-control" name="jenis_kelamin">
                  <option value="" selected="">Pilih Jenis Kelamin</option>
                  <option value="Laki-laki" <?php echo $pendaftar[0]->jenis_kelamin == 'Laki-laki' ? 'selected' : '' ?>>Laki-laki</option>
                  <option value="Perempuan" <?php echo $pendaftar[0]->jenis_kelamin == 'Perempuan' ? 'selected' : '' ?>>Perempuan</option>
                </select>  
              </div>

              <div class="form-group t-left">
                <label for="pwd">Jenis Lomba:</label>
                <select class="form-control" name="kategori_lomba">
                  <option value="" selected="">Pilih Jenis Lomba</option>
                  <?php foreach ($kategori as $row) { ?>
                    <option value="<?php echo $row->nama_kategori ?>" <?php echo $pendaftar[0]->kategori_lomba == $row->nama_kategori ? 'selected' : '' ?> ><?php echo $row->nama_kategori ?></option>
                  <?php } ?>
                </select>  
              </div>

            </div>
            <div class="ln_solid"></div>
  <!--           <div class="form-group no-print">
                <button type="submit" class="btn btn-success">Submit</button>
            </div> -->
          </form>
        </div>
      </div>
    </div>

  </div>
</div>


<style type="text/css">
  @media print {
     .no-print {
        display: none;

     }
  }
</style>
<script type="text/javascript">
  document.getElementById("doPrint").addEventListener("click", function() {
     var printContents = document.getElementById('printDiv').innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
});
</script>


<?php $this->load->view('admin/component/footer'); ?> 
