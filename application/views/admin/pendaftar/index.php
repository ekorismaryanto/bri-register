<?php

$this->load->view('admin/component/header');
$this->load->view('admin/component/sidebar');
$this->load->view('admin/component/top_nav');

?>
<style>

    @media print {
           .no-print {
              display: none;
           }
        }

</style>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Master Data Pendaftar <small> <!-- <a href="<?php echo site_url('admin/setting/createCategori') ?>"><button class="btn btn-danger">Tambah Pendaftar</button></a> --> </small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>No Peserta</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>No hp</th>
                <th>Alamat</th>
                <th>Usia</th>
                <th>Jenis Lomba</th>
                <th class="no-print">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($pendaftar as $row) {?>
              <tr>
                <td><?php echo $row->id_akun ?></td>
                <td><?php echo $row->nik ?></td>
                <td><?php echo $row->nama ?></td>
                <td><?php echo $row->no_hp ?></td>
                <td><?php echo $row->alamat ?></td>
                <td><?php echo $row->usia ?></td>
                <td><?php echo $row->kategori_lomba ?></td>
                <td class="no-print">
                  <a href="<?php echo site_url('admin/pendaftar/detail/'. $row->id) ?>"> <span class="fa fa-pencil"></span> Detail</a> 
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('admin/component/footer'); ?> 
