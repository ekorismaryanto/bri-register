<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aplikasi Pendaftaran </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'assets/'; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url().'assets/'; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url().'assets/'; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url().'assets/'; ?>vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url().'assets/'; ?>build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <style type="text/css">
        .t-left{
            text-align: left !important
        }
         .t-center{
            text-align: center !important
        }
        .login_content{
          padding :0px !important;
        }
      </style>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <h4 class="t-center">ADMIN</h4>
          <section class="login_content">
            <form method="post" action="<?php echo site_url('admin/akun/doLogin') ?>">
              <h1>Login</h1>
          
              <div class="form-group t-left">
                <label for="nama" >Email:</label>
                <input type="email" class="form-control" id="nama" required="" name="email">
              </div>

              <div class="form-group t-left">
                <label for="no_hp" >Password</label>
                <input type="password" class="form-control" id="nama" required="" name="password">
              </div>

              <div>
                <button type="submit" class="btn btn-danger">Log in</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div>
                  <h1>Your Company</h1>
                  <p>©2019, Name Company, <?php echo date('d-F-Y h:i:s') ?></p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
